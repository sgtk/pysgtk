PySGTK
======

A Python implementation of the "Simple Grid Toolkit".

## Requirements ##
* `Python 3`
* `SDL2`
* `SDL2_ttf`
* `SDL2_image`
* `PySDL2`

Check out the `example.py` script to see how to use this toolkit

**WIP!**
