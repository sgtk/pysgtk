from pathlib import Path
from ctypes import POINTER, c_int, byref
from sdl2 import (
    SDL_Renderer, SDL_SetRenderDrawColor, SDL_RenderClear,
    SDL_RenderPresent, SDL_Rect, SDL_RenderFillRect,
    SDL_CreateTextureFromSurface, SDL_DestroyTexture, SDL_FreeSurface,
    SDL_QueryTexture, SDL_Color, SDL_RenderCopy, SDL_Texture, SDL_CreateTexture,
    SDL_PIXELFORMAT_RGBA32, SDL_TEXTUREACCESS_TARGET, SDL_SetRenderTarget,
    SDL_SetRenderDrawBlendMode, SDL_BLENDMODE_NONE, SDL_BLENDMODE_BLEND,
    SDL_GetRenderTarget
)
from sdl2.sdlttf import TTF_RenderUTF8_Blended
from sdl2.sdlimage import IMG_LoadTexture

from .Font import Font

_texture_pointer = POINTER(SDL_Texture)


class TargetTextureContext:
    def __init__(self, painter, target_texture: _texture_pointer):
        self._painter = painter
        self._target_texture = target_texture
        self._prev_target_texture: _texture_pointer = None
    def __enter__(self):
        self._prev_target_texture = self._painter.get_target_texture()
        self._painter.set_target_texture(self._target_texture)
    def __exit__(self, *_):
        self._painter.set_target_texture(self._prev_target_texture)


class Painter:
    def __init__(self, ren: POINTER(SDL_Renderer), font: Font):
        self.ren = ren
        self._font = font
        self.tk = self._font.tk

        self._src_rect = SDL_Rect()
        self._dest_rect = SDL_Rect()
        self._font_color = SDL_Color()

        self.set_draw_blend_mode(SDL_BLENDMODE_BLEND)

    def create_target_texture(self, w: int, h: int) -> _texture_pointer:
        tex = SDL_CreateTexture(
            self.ren, SDL_PIXELFORMAT_RGBA32, SDL_TEXTUREACCESS_TARGET, w, h
        )
        if not tex: self.tk.check_sdl2_error("SDL_CreateTexture")
        return tex

    def load_image(self, file_path: Path) -> _texture_pointer:
        tex = IMG_LoadTexture(self.ren, str(file_path.resolve()).encode())
        if not tex: self.tk.check_sdl2_error("IMG_LoadTexture", do_exit=True)
        return tex

    def clear(self, color: tuple):
        # disable blending so the pixels get replaced by the color
        self.set_draw_blend_mode(SDL_BLENDMODE_NONE)
        self._set_color(color)
        if SDL_RenderClear(self.ren) < 0:
            self.tk.check_sdl2_error("SDL_RenderClear")
        # restore blend mode
        self.set_draw_blend_mode(SDL_BLENDMODE_BLEND)

    def rect(self, color: tuple, x: int, y: int, w: int, h: int):
        d = self._dest_rect
        d.x, d.y, d.w, d.h = x, y, w, h
        self._set_color(color)
        if SDL_RenderFillRect(self.ren, d) < 0:
            self.tk.check_sdl2_error("SDL_RenderFillRect")

    def rect_outline(self, color: tuple, thickness: int, x: int, y: int, w: int, h: int):
        # |-|
        # | |
        # |-|
        for rect in (
            (x, y, thickness, h), # left vertical bar
            (x + w - thickness, y, thickness, h), # right vertical bar
            (x + thickness, y, w - 2 * thickness, thickness), # upper horizontal bar
            (x + thickness, y + h - thickness, w - 2 * thickness, thickness) # lower horizontal bar
        ): self.rect(color, *rect)

    def text(self, text: str, font_name: str, font_size: int, color: tuple, x: int, y: int):
        # get font
        if text == "": return
        font = self._font.get(font_name, font_size)

        # set color
        c = self._font_color
        if len(color) == 3:
            c.r, c.g, c.b = color
            c.a = 255
        else: c.r, c.g, c.b, c.a = color

        # render text to surface
        text_sf = TTF_RenderUTF8_Blended(font, text.encode(), c)
        if not text_sf:
            self.tk.check_sdl2_error("TTF_RenderUTF8_Blended")
        # create texture from surface
        text_texture = SDL_CreateTextureFromSurface(self.ren, text_sf)
        if not text_texture:
            self.tk.check_sdl2_error("SDL_CreateTextureFromSurface")
        # delete surface
        SDL_FreeSurface(text_sf)

        # render text texture
        self.texture(text_texture, x, y)

        # destroy texture
        SDL_DestroyTexture(text_texture)

    def get_texture_size(self, texture: _texture_pointer) -> tuple:
        c_w = c_int(0)
        c_h = c_int(0)
        if SDL_QueryTexture(texture, None, None, byref(c_w), byref(c_h)) < 0:
            self.tk.check_sdl2_error("SDL_QueryTexture")
        return (c_w.value, c_h.value)

    def set_target_texture(self, target_texture: _texture_pointer):
        if SDL_SetRenderTarget(self.ren, target_texture) < 0:
            self.tk.check_sdl2_error("SDL_SetRenderTarget")

    def get_target_texture(self) -> _texture_pointer:
        return SDL_GetRenderTarget(self.ren)

    def target_texture(self, target_texture: _texture_pointer) -> TargetTextureContext:
        return TargetTextureContext(self, target_texture)

    def set_draw_blend_mode(self, sdl2_blend_mode: int):
        if SDL_SetRenderDrawBlendMode(self.ren, sdl2_blend_mode) < 0:
            self.tk.check_sdl2_error("SDL_SetRenderDrawBlendMode")

    def texture(self, texture: _texture_pointer, x: int, y: int, w:int=-1, h:int=-1, src:tuple=None):
        if w == -1 or h == -1:
            tex_w, tex_h = self.get_texture_size(texture)
            if w == -1: w = tex_w
            if h == -1: h = tex_h

        d = self._dest_rect
        d.x, d.y, d.w, d.h = x, y, w, h

        s = None
        if src is not None:
            s = self._src_rect
            s.x, s.y, s.w, s.h = src

        if SDL_RenderCopy(self.ren, texture, s, d) < 0:
            self.tk.check_sdl2_error("SDL_RenderCopy")

    def present(self):
        SDL_RenderPresent(self.ren)

    def _set_color(self, color: tuple):
        """ color: tuple - (r: int, g: int, b: int, a:int=255) """
        if len(color) == 3: r = SDL_SetRenderDrawColor(self.ren, *color, 255)
        else: r = SDL_SetRenderDrawColor(self.ren, *color)
        if r < 0: self.tk.check_sdl2_error("SDL_SetRenderDrawColor")
