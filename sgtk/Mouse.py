from ctypes import POINTER, c_int, byref
from sdl2 import (
    SDL_Event, SDL_MOUSEMOTION, SDL_MOUSEBUTTONUP, SDL_MOUSEBUTTONDOWN,
    SDL_MOUSEWHEEL, SDL_Cursor, SDL_CreateSystemCursor, SDL_SetCursor,
    SDL_SYSTEM_CURSOR_IBEAM, SDL_SYSTEM_CURSOR_ARROW, SDL_SYSTEM_CURSOR_HAND,
    SDL_FreeCursor, SDL_GetMouseState
)
from .BaseWidget import BaseWidget

from .constants import (
    MOUSE_LEFT, MOUSE_MIDDLE, MOUSE_RIGHT, MOUSE_MOTION, MOUSE_DOWN,
    MOUSE_UP, MOUSE_WHEEL_Y, MOUSE_WHEEL_X, WINDOW_CLOSED
)

_cursor_ptr = POINTER(SDL_Cursor)

mouse_sdl2_events = (SDL_MOUSEMOTION, SDL_MOUSEWHEEL, SDL_MOUSEBUTTONDOWN, SDL_MOUSEBUTTONUP)

class Mouse:
    def __init__(self, sgtk):
        """ sgtk: SGTK() """
        self.tk = sgtk

        self.left = False
        self.middle = False
        self.right = False

        self.x = 0
        self.y = 0
        self.dx = 0
        self.dy = 0
        self.scroll_dx = 0
        self.scroll_dy = 0

        # TODO: rename 'selected' to 'hovered' or smth like that
        self.selected: BaseWidget = None
        self._win = None # Window()
        self._window_just_closed = False

        self._cursors = {name: None for name in ("arrow", "ibeam", "pointer")}

        self.tk.event.listen(WINDOW_CLOSED, self._on_window_close)

    def init_cursors(self):
        for name, cursor in {
            "arrow":   SDL_SYSTEM_CURSOR_ARROW,
            "ibeam":   SDL_SYSTEM_CURSOR_IBEAM,
            "pointer": SDL_SYSTEM_CURSOR_HAND
        }.items(): self._cursors[name] = SDL_CreateSystemCursor(cursor)
        self.tk.check_sdl2_error("SDL_CreateSystemCursor")

    def flush(self):
        self.dx = 0
        self.dy = 0
        self.scroll_dx = 0
        self.scroll_dy = 0
        self._window_just_closed = False

    def is_pressed(self, button: int) -> bool:
        return {
            MOUSE_LEFT: self.left,
            MOUSE_MIDDLE: self.middle,
            MOUSE_RIGHT: self.right
        }[button]

    def update_pos(self):
        c_x = c_int(0)
        c_y = c_int(0)
        SDL_GetMouseState(byref(c_x), byref(c_y))
        self._update_pos(round(c_x.value / self.tk.window_scale), round(c_y.value / self.tk.window_scale))

    def handle_event(self, sdl2_event: SDL_Event):
        if self._window_just_closed: return
        event_type = sdl2_event.type

        if event_type == SDL_MOUSEMOTION:
            event = sdl2_event.motion
        elif event_type in (SDL_MOUSEBUTTONDOWN, SDL_MOUSEBUTTONUP):
            event = sdl2_event.button
        elif event_type == SDL_MOUSEWHEEL:
            event = sdl2_event.wheel

        win = self.tk.get_window_from_id(event.windowID)
        if self._win is not win:
            self.left = self.middle = self.right = False
        self._win = win

        if event_type == SDL_MOUSEMOTION:
            self._update_pos(event.x, event.y)
            self._emit_event(MOUSE_MOTION, None)

        elif event_type in (SDL_MOUSEBUTTONDOWN, SDL_MOUSEBUTTONUP):
            button = event.button - 1
            is_down = event_type == SDL_MOUSEBUTTONDOWN

            if button == MOUSE_LEFT:   self.left   = is_down
            if button == MOUSE_MIDDLE: self.middle = is_down
            if button == MOUSE_RIGHT:  self.right  = is_down

            self._emit_event(MOUSE_DOWN if is_down else MOUSE_UP, button)

            if self.selected is not None and is_down:
                self.selected.on_mouse_down(button)

        elif event_type == SDL_MOUSEWHEEL:
            # add a limit. Just in case
            max_abs = 5
            if event.x != 0:
                self.scroll_dx = event.x
                if   self.scroll_dx < -max_abs: self.scroll_dx = -max_abs
                elif self.scroll_dx >  max_abs: self.scroll_dx =  max_abs
                self._emit_event(MOUSE_WHEEL_X, self.scroll_dx)
            if event.y != 0:
                self.scroll_dy = event.y
                if   self.scroll_dy < -max_abs: self.scroll_dy = -max_abs
                elif self.scroll_dy >  max_abs: self.scroll_dy =  max_abs
                self._emit_event(MOUSE_WHEEL_Y, self.scroll_dy)

    def _update_pos(self, x: int, y: int):
        if self._win is None: return

        prev_x = self.x
        prev_y = self.y
        self.x = x
        self.y = y
        self.dx = self.x - prev_x
        self.dy = self.y - prev_y

        # update selected Base
        prev_selected = self.selected
        self.selected = self._win.get_widget_at(self.x, self.y)

        if prev_selected is not self.selected:
            if prev_selected is not None:
                prev_selected.on_unhover()
            if self.selected is not None:
                self.selected.on_hover()

    def _emit_event(self, event_type: int, data: object):
        if self._win is not None: self._win.event.emit(event_type, data)
        self.tk.event.emit(event_type, data)

    def set_cursor(self, cursor_name: str):
        SDL_SetCursor(self._cursors.get(cursor_name))

    def close(self):
        for cursor in self._cursors.values():
            SDL_FreeCursor(cursor)

    def _on_window_close(self, _window):
        self._window_just_closed = True
        self._win = None
