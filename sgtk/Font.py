from pathlib import Path
from ctypes import POINTER, c_int, byref
from sdl2.sdlttf import (
    TTF_Init, TTF_Quit, TTF_OpenFont, TTF_CloseFont, TTF_Font, TTF_SizeUTF8
)

class Font:
    def __init__(self, sgtk, font_dir_path: Path):
        """ sgtk: SGTK """

        self.tk = sgtk
        self._font_dir_path = font_dir_path

        self._fonts = {}

        self._init_sdl2_ttf()

    def _load(self, name: str, size: int) -> POINTER(TTF_Font):
        font_file_path_str = str(self._font_dir_path.joinpath(f"{name}.ttf"))
        font = TTF_OpenFont(font_file_path_str.encode(), size)
        if font is None: self.tk.check_sdl2_error("TTF_OpenFont", do_exit=True)
        return font

    def get(self, name: str, size: int) -> POINTER(TTF_Font):
        font = self._fonts.get((name, size))
        if font is None:
            font = self._load(name, size)
            self._fonts[(name, size)] = font
        return font

    def size(self, text: str, name: str, size: int) -> tuple:
        """ Returns: (w: int, h: int) """
        font = self.get(name, size)
        w = c_int(0)
        h = c_int(0)
        TTF_SizeUTF8(font, text.encode(), byref(w), byref(h))
        return (w.value, h.value)

    def _init_sdl2_ttf(self):
        if TTF_Init() == -1:
            self.tk.check_sdl2_error("TTF_Init", do_exit=True)

    def close(self):
        for font in self._fonts.values():
            TTF_CloseFont(font)
        TTF_Quit()
