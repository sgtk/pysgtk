
class EventManager:
    def __init__(self):
        self._listeners = {}

    def emit(self, event_type: int, data):
        for callback in tuple(self._listeners.get(event_type, [])):
            callback(data)

    def listen(self, event_type: int, callback: callable):
        callback_list = self._listeners.get(event_type)
        if callback_list is None:
            callback_list = []
            self._listeners[event_type] = callback_list
        if callback in callback_list: return
        callback_list.append(callback)

    def unlisten(self, event_type: int, callback: callable):
        callback_list = self._listeners.get(event_type)
        if callback_list is None or callback not in callback_list:
            return
        callback_list.remove(callback)
