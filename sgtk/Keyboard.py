from ctypes import POINTER
from sdl2 import (
    SDL_Event, SDL_TEXTINPUT, SDL_KEYDOWN, SDL_KEYUP,
    SDLK_LSHIFT, SDLK_RSHIFT, SDLK_LCTRL, SDLK_RCTRL, SDLK_LALT, SDLK_RALT,
    SDL_GetKeyboardState, SDL_GetScancodeFromKey, Uint8
)
keyboard_sdl2_events = (SDL_TEXTINPUT, SDL_KEYUP, SDL_KEYDOWN)

from .constants import KEYBOARD_CHAR, KEYBOARD_PRESS_KEY, KEYBOARD_RELEASE_KEY, WINDOW_CLOSED

class Keyboard:
    def __init__(self, sgtk):
        """ sgtk: SGTK() """

        self.tk = sgtk
        self._win = None # Window()
        self._mods = {"ctrl": False, "shift": False, "alt": False}
        self._state = POINTER(Uint8)
        self._window_just_closed = 0

        self._mods_map = {}
        for sym, sdl_syms in {
            "shift": (SDLK_LSHIFT, SDLK_RSHIFT),
            "ctrl":  (SDLK_LCTRL, SDLK_RCTRL),
            "alt":   (SDLK_LALT, SDLK_RALT)
        }.items():
            for sdl_sym in sdl_syms: self._mods_map[sdl_sym] = sym

        self.tk.event.listen(WINDOW_CLOSED, self._on_window_close)

    def update(self):
        self._state = SDL_GetKeyboardState(None)
        if self._window_just_closed:
            self._window_just_closed = False

    def is_pressed(self, sdl2_keycode: int) -> bool:
        return bool(self._state[SDL_GetScancodeFromKey(sdl2_keycode)])

    def get_mod(self, mod_name: str) -> bool:
        return self._mods[mod_name]

    def _update_mod(self, sdl2_sym: int, is_pressed: bool):
        sym = self._mods_map.get(sdl2_sym)
        if sym is not None: self._mods[sym] = is_pressed

    def handle_event(self, sdl2_event: SDL_Event):
        if self._window_just_closed: # to debounce the keyboard after a window closed
            return

        event_type = sdl2_event.type
        if event_type == SDL_TEXTINPUT: self._handle_text_input_event(sdl2_event)
        elif event_type in (SDL_KEYDOWN, SDL_KEYUP): self._handle_key(sdl2_event)

    def _handle_key(self, sdl2_event: SDL_Event):
        event = sdl2_event.key
        keysym = event.keysym.sym

        self._win = self.tk.get_window_from_id(event.windowID)

        if event.type == SDL_KEYDOWN:
            self._update_mod(keysym, True)
            self._emit_event(KEYBOARD_PRESS_KEY, keysym)
        else:
            self._update_mod(keysym, False)
            self._emit_event(KEYBOARD_RELEASE_KEY, keysym)

    def _handle_text_input_event(self, sdl2_event: SDL_Event):
        event = sdl2_event.text
        text = event.text.decode()
        self._win = self.tk.get_window_from_id(event.windowID)
        self._emit_event(KEYBOARD_CHAR, text)

    def _emit_event(self, event_type: int, data: object):
        if self._win is not None: self._win.event.emit(event_type, data)
        self.tk.event.emit(event_type, data)

    def _on_window_close(self, _window):
        self._window_just_closed = True
