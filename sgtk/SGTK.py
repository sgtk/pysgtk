import sys
from pathlib import Path
from sdl2 import (
    SDL_Init, SDL_Quit, SDL_INIT_VIDEO, SDL_Delay, SDL_GetError,
    SDL_Event, SDL_PollEvent, SDL_QUIT,
    SDL_WINDOWEVENT, SDL_WINDOWEVENT_CLOSE, SDL_GetTicks,
    SDL_SetClipboardText, SDL_GetClipboardText, SDL_HasClipboardText
)
from sdl2.sdlimage import IMG_Init, IMG_INIT_PNG, IMG_Quit

from .Mouse import Mouse, mouse_sdl2_events
from .Keyboard import Keyboard, keyboard_sdl2_events
from .Font import Font
from .ParentWidget import ParentWidget
from .EventManager import EventManager
from .constants import SGTK_UPDATE

class SGTK(ParentWidget):
    THIS_DIR = Path(__file__).parent.resolve()
    def __init__(self, accelerated:bool=False, window_scale:float=1.0):
        self.accelerated: bool = accelerated
        self.window_scale: float = window_scale

        self.event = EventManager()
        self.mouse = Mouse(self)
        self.keyboard = Keyboard(self)
        self.font = Font(self, self.THIS_DIR.joinpath("fonts"))
        self.runtime_secs = 0.0

        super().__init__()
        self.setup_sgtk(self, None)

        self._event = SDL_Event()
        self._running = True

        self._delay_fps = 60

        self._init_sdl2()
        self.mouse.init_cursors()

    def run(self):
        self.mouse.update_pos()
        self.request_redraw()
        self.render() # full redraw
        while self._running: self.update_tk()
        self.close()

    def stop(self):
        self._running = False

    def update(self): pass

    def update_tk(self):
        self.runtime_secs = SDL_GetTicks() / 1000

        self.mouse.flush()

        do_render = False
        e = self._event
        while SDL_PollEvent(e):
            if e.type in mouse_sdl2_events:
                self.mouse.handle_event(e)
            elif e.type in keyboard_sdl2_events:
                self.keyboard.handle_event(e)
            elif e.type == SDL_WINDOWEVENT:
                # in the future this should be handled in Window
                do_render = True
                win_event = e.window
                if win_event.event == SDL_WINDOWEVENT_CLOSE:
                    win = self.get_window_from_id(win_event.windowID)
                    if win is not None: win.on_close_request()
                self.mouse.update_pos()
            # when the last window closes
            # or a signal to close was sent to the program
            elif e.type == SDL_QUIT and self.should_close():
                self.stop()

        self.keyboard.update()

        self.event.emit(SGTK_UPDATE, self.runtime_secs)
        self.update()

        if self._redraw_queue: self.redraw()
        if do_render: self.render()
        for win in self._children: win.paint.present()

        SDL_Delay(1000 // self._delay_fps)

    def get_window_from_id(self, win_id: int) -> ParentWidget:
        """ Returns: Window() or None """
        for window in tuple(self._children):
            if window.win_id == win_id: return window
        return None

    def check_sdl2_error(self, sdl2_func_name: str, do_exit:bool=False):
        err = SDL_GetError().decode()
        if err == "": return
        print(f"{'FATAL - ' if do_exit else ''}{sdl2_func_name}: {err}", file=sys.stderr)
        if do_exit:
            self.close()
            exit(2)

    def _add_child_to_graph(self, graph_lines: list, child):
        parent = child.parent
        if parent is not None:
            graph_lines.append(
                '{}["{}"] --> {}["{}"];'.format(
                    parent.get_id(), str(parent).replace('"', "#quot;"),
                    child.get_id(), str(child).replace('"', "#quot;")
                )
            )
        if isinstance(child, ParentWidget):
            for child in child.get_children():
                self._add_child_to_graph(graph_lines, child)

    def create_graph(self, file_path:str="./sgtk_graph.md"):
        graph_lines = []
        self._add_child_to_graph(graph_lines, self)

        text_lines = [
            "SGTK Tree",
            "=========",
            "",
            "```mermaid",
            "graph LR;",
            *graph_lines,
            "```"
        ]

        with Path(file_path).resolve().open("w") as file:
            file.write("\n".join(text_lines))

    def add_child(self, child):
        """ child: Window()
        It is recommended to add the Window when it's already decorated """
        # draw to both buffers of the window double buffer
        super().add_child(child)
        child.request_redraw()
        for _ in range(2):
            child.render()
            child.paint.present()

    def child_updated_rect(self, child):
        self.mouse.update_pos()

    def set_sys_clipboard(self, data: str):
        SDL_SetClipboardText(data.encode())

    def get_sys_clipboard(self) -> str:
        """ Returns `str` or `None` if there is no text in the clipboard """
        if not SDL_HasClipboardText(): return None
        return SDL_GetClipboardText().decode()

    def _close(self, update:bool=True):
        super()._close(update=False)
        self.font.close()
        self.mouse.close()
        IMG_Quit()
        SDL_Quit()

    # override when needed
    def should_close(self) -> bool: return True

    def _init_sdl2(self):
        if SDL_Init(SDL_INIT_VIDEO) < 0:
            self.check_sdl2_error("SDL_Init", do_exit=True)

        img_flags = IMG_INIT_PNG
        if img_flags & IMG_Init(img_flags) != img_flags:
            self.check_sdl2_error("IMG_Init", do_exit=True)
