from .Label import (
    Label, LABEL_COLOR, LABEL_BG_COLOR, LABEL_FONT_NAME,
)
from .Window import Window
from .constants import MOUSE_LEFT

def do_nothing(): pass

BUTTON_TEXT = "Button"
BUTTON_FONT_SIZE = 12
BUTTON_IPAD = 4
BUTTON_CMD = do_nothing
BUTTON_HL_BG_COLOR = (100, 100, 100)
BUTTON_DEACTIVE_COLOR = (130, 130, 130)
BUTTON_DEACTIVE_BG_COLOR = (100, 30, 30)

class Button(Label):
    def __init__(self, win: Window, text:str=BUTTON_TEXT, cmd:callable=BUTTON_CMD,
        font_name:str=LABEL_FONT_NAME, font_size:int=BUTTON_FONT_SIZE,
        color:tuple=LABEL_COLOR, bg_color:tuple=LABEL_BG_COLOR, hl_bg_color:tuple=BUTTON_HL_BG_COLOR,
        deactive_bg_color:tuple=BUTTON_DEACTIVE_BG_COLOR, deactive_color:tuple=BUTTON_DEACTIVE_COLOR,
        x:int=0, y:int=0, pad_x:int=0, pad_y:int=0,
        ipad_x:int=BUTTON_IPAD, ipad_y:int=BUTTON_IPAD
    ):
        super().__init__(
            win, text=text, font_name=font_name, font_size=font_size,
            color=color, bg_color=bg_color, x=x, y=y, pad_x=pad_x, pad_y=pad_y,
            ipad_x=ipad_x, ipad_y=ipad_y
        )

        self._cmd = cmd
        self._hl_bg_color = hl_bg_color
        self._default_color = color
        self._default_bg_color = bg_color
        self._deactive_color = deactive_color
        self._deactive_bg_color = deactive_bg_color
        self._active = True

    def set_cmd(self, cmd: callable):
        self._cmd = cmd

    def is_active(self) -> bool: return self._active

    def activate(self):
        if self._active: return
        self._active = True
        self._color = self._default_color
        self._bg_color = self._default_bg_color
        self.request_redraw()

    def deactivate(self):
        if not self._active: return
        self._active = False
        self._color = self._deactive_color
        self._bg_color = self._deactive_bg_color
        self.request_redraw()

    def on_mouse_down(self, button: int):
        if self._active and button == MOUSE_LEFT:
            self._cmd()

    def on_hover(self):
        if not self._active: return
        self.tk.mouse.set_cursor("pointer")
        self._bg_color = self._hl_bg_color
        self.request_redraw()

    def on_unhover(self):
        self.tk.mouse.set_cursor("arrow")
        if not self._active: return
        self._bg_color = self._default_bg_color
        self.request_redraw()
