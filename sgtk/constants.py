MOUSE_LEFT    = 0
MOUSE_MIDDLE  = 1
MOUSE_RIGHT   = 2

# EVENTS #
MOUSE_DOWN    = 1 # (button: int)
MOUSE_UP      = 2 # (button: int)
MOUSE_MOTION  = 3 # (None)
MOUSE_WHEEL_X = 4 # (scroll_delta_x: int)
MOUSE_WHEEL_Y = 5 # (scroll_delta_y: int)

KEYBOARD_CHAR        = 6 # (char: str)
KEYBOARD_PRESS_KEY   = 7 # (sdl2_keycode: int)
KEYBOARD_RELEASE_KEY = 8 # (sdl2_keycode: int)

SGTK_UPDATE = 9 # (runtime_secs: float)

PATH_OPEN = 10     # (explorer_dialog: dialogs.BaseExplorer.BaseExplorer)
USER_DECISION = 11 # (dialog: AskYesOrCancel)

WINDOW_CLOSED = 12 # (window: Window)
