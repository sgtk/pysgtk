from pathlib import Path
from .BaseExplorer import BaseExplorer, SELECT_FILE_EVENT
from .AskYesOrCancel import AskYesOrCancel
from .. import SGTK
from ..constants import USER_DECISION

class SaveFile(BaseExplorer):
    def __init__(self, sgtk: SGTK, file_type: str, title:str="Save File",
        ok_btn_text:str="Save", start_dir:Path=None, show_hidden:bool=False,
        ensure_file_type:bool=False, AlreadyExistsDialog:AskYesOrCancel=AskYesOrCancel, **theme
    ):
        super().__init__(sgtk, [file_type], title, ok_btn_text=ok_btn_text,
            start_dir=start_dir, show_hidden=show_hidden, **theme
        )
        self._ensure_file_type = ensure_file_type

        self._AlreadyExistsDialog = AlreadyExistsDialog
        self._ask_yes_or_cancel_dialog: AskYesOrCancel = None
        self._overwrite_allowed = False

        self.tk.event.listen(USER_DECISION, self._on_user_decision)

    def _ok(self):
        text = self._path_input.get_text()
        path = Path(text)
        if not self._is_path_valid(path): return

        if self._ensure_file_type:
            file_type = "." + ".".join(self._file_types[0].split(".")[1:])
            if not path.name.endswith(file_type):
                path = path.with_name(path.name + file_type)

        if self._path != path:
            self.event.emit(SELECT_FILE_EVENT, path)
        elif self._path.exists() and not self._overwrite_allowed:
            if self._ask_yes_or_cancel_dialog is not None: return
            self._ok_btn.deactivate()
            self._ask_yes_or_cancel_dialog = self._AlreadyExistsDialog(self.tk,
                title="File already exists", text=f'Do you really want to overwrite this file? "{self._path}"'
            )

        else:
            super()._ok()

    def _is_path_valid(self, path: Path) -> bool:
        if path is None: return False
        return not path.is_dir() and path.parent.exists() and self.can_read(path.parent)

    def _on_user_decision(self, dialog: AskYesOrCancel):
        if dialog is not self._ask_yes_or_cancel_dialog: return
        self._ok_btn.activate()
        self._ask_yes_or_cancel_dialog = None

        self._overwrite_allowed = dialog.get_decision()
        if self._overwrite_allowed: self._ok()

    def _win_close(self, update:bool=True):
        self.tk.event.unlisten(USER_DECISION, self._on_user_decision)
        super()._win_close(update=update)
