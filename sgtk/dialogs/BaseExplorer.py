import os
from pathlib import Path
from sdl2 import SDLK_h, SDLK_ESCAPE, SDLK_TAB, SDLK_BACKSPACE

from .. import (
    SGTK, Window, Frame, Button, TextInputLine
)
from ..EventManager import EventManager
from ..constants import KEYBOARD_PRESS_KEY, PATH_OPEN
from ..Label import LABEL_COLOR, LABEL_BG_COLOR

SET_DIR_EVENT     = 123456 # (dir_path: Path)
SELECT_FILE_EVENT = 123457 # (file_path: Path)

BASE_EXPLORER_SELECTED_BG_COLOR = (90, 90, 170)
BASE_EXPLORER_DIR_COLOR         = (150, 150, 255)


class PathButton(Button):
    def __init__(self, base_explorer, path: Path, **kwargs):
        self._base_explorer = base_explorer
        self._path = path
        super().__init__(self._base_explorer.win, **{**kwargs, "text": self.get_path_text()})

        self.set_cmd(self.cmd)

    def get_path_text(self) -> str:
        raise NotImplementedError
    def cmd(self):
        raise NotImplementedError

class FilePathButton(PathButton):
    def get_path_text(self) -> str:
        return self._path.name
    def cmd(self):
        self._base_explorer.event.emit(SELECT_FILE_EVENT, self._path)

class DirectoryPathButton(PathButton):
    def get_path_text(self) -> str:
        return self._path.name + "/"
    def cmd(self):
        self._base_explorer.event.emit(SET_DIR_EVENT, self._path)


class BaseExplorer:
    def __init__(self, sgtk: SGTK, file_types: list, title: str,
        ok_btn_text:str="OK", start_dir:Path=None, show_hidden:bool=False,

        dir_color:tuple=BASE_EXPLORER_DIR_COLOR,
        selected_bg_color:tuple=BASE_EXPLORER_SELECTED_BG_COLOR,

        window_theme:dict=None, nav_bar_theme:dict=None,
        nav_bar_btn_theme:dict=None, nav_bar_path_theme:dict=None,
        explorer_theme:dict=None, explorer_btn_theme:dict=None,
        action_bar_theme:dict=None, action_bar_btn_theme:dict=None
    ):
        self.tk = sgtk
        self._path: Path = None
        self._dir: Path = None
        self._file_types = file_types
        self._show_hidden = show_hidden
        self._ok_btn_text = ok_btn_text

        self._buttons = {}
        self._explorer_frame: Frame = None
        self._ok_btn: Button = None

        # I'm not really happy with this but it works
        self._window_theme = window_theme or {}
        self._nav_bar_theme = nav_bar_theme or {}
        self._nav_bar_btn_theme = nav_bar_btn_theme or {}
        self._nav_bar_path_theme = nav_bar_path_theme or {}
        self._explorer_theme = explorer_theme or {}
        self._explorer_btn_theme = explorer_btn_theme or {}
        self._action_bar_theme = action_bar_theme or {}
        self._action_bar_btn_theme = action_bar_btn_theme or {}

        self._default_btn_bg_color = self._explorer_btn_theme.get("bg_color") or LABEL_BG_COLOR
        self._default_btn_color    = self._explorer_btn_theme.get("color") or LABEL_COLOR
        self._selected_btn_bg_color = selected_bg_color
        self._dir_color = dir_color

        self.win: Window = None
        self._path_input: TextInputLine = None
        self.event = EventManager()

        self.event.listen(SET_DIR_EVENT, self._on_set_dir_event)
        self.event.listen(SELECT_FILE_EVENT, self._on_select_file_event)

        if start_dir is None: start_dir = Path.home()
        self._setup_win(title, start_dir.resolve())

    def get_path(self) -> Path:
        """ Returns None if the user canceled """
        return self._path
    def get_dir(self) -> Path:
        return self._dir

    def _is_path_valid(self, path: Path) -> bool:
        """ path: Optional[Path] """
        raise NotImplementedError()

    def _cancel(self):
        self.event.emit(SELECT_FILE_EVENT, None)
        self.win.close(update=False)

    def _ok(self):
        if not self._is_path_valid(self._path): return
        self.win.close(update=False)

    def _on_set_dir_event(self, dir_path: Path):
        self._dir = dir_path
        self.event.emit(SELECT_FILE_EVENT, None)

    def _on_select_file_event(self, file_path: Path):
        prev_selected_btn = self._buttons.get(self._path)
        if prev_selected_btn is not None:
            prev_selected_btn.activate()
            prev_selected_btn.set_bg_color(self._default_btn_bg_color)

        selected_btn = self._buttons.get(file_path)
        if selected_btn is not None:
            selected_btn.deactivate()
            selected_btn.set_bg_color(self._selected_btn_bg_color)

        self._path = file_path

    def _path_input_on_key_enter(self):
        text = self._path_input.get_text()
        p = Path(text)
        prev_path = self._path

        if p.is_dir() and self.can_read(p):
            self.event.emit(SET_DIR_EVENT, p)

        elif p.parent.is_dir() and self.can_read(p.parent):
            self.event.emit(SET_DIR_EVENT, p.parent)

            if self._is_path_valid(p):
                self.event.emit(SELECT_FILE_EVENT, p)
                if p == prev_path: # user pressed Enter twice, so open that file
                    self._ok()
            else:
                self._path_input.set_text(text) # restore input text

    def _explorer_on_set_dir_event(self, dir_path: Path):
        MAX_ROWS = 15

        if self._explorer_frame is not None:
            self._explorer_frame.close(update=False)

        self._buttons.clear()
        for child_path in self._get_dir_content(dir_path):
            if child_path.is_dir():
                btn = DirectoryPathButton(self, child_path, **{**self._explorer_btn_theme, "color": self._dir_color})
            else:
                btn = FilePathButton(self, child_path, **{**self._explorer_btn_theme, "color": self._default_btn_color})
            if not self.can_read(child_path): btn.deactivate()
            self._buttons[child_path] = btn

        self._explorer_frame = Frame(self.win,
            num_cols=len(self._buttons)//MAX_ROWS+1, num_rows=MAX_ROWS,
            **self._explorer_theme
        )
        row = 0
        col = 0
        for button in self._buttons.values():
            self._explorer_frame.add_grid_child(button, col=col, row=row)
            row += 1
            if row == MAX_ROWS:
                row = 0
                col += 1
        self.win.add_grid_child(self._explorer_frame, row=1)

    def _setup_win(self, title: str, initial_dir: Path):
        self.win = Window(self.tk, title=title, num_rows=3, **self._window_theme)

        # Action Buttons
        cancel_btn = Button(self.win, text="Cancel", cmd=self._cancel, **self._action_bar_btn_theme)
        self._ok_btn = Button(self.win, text=self._ok_btn_text, cmd=self._ok, **self._action_bar_btn_theme)

        def ok_btn_on_select_file_event(file_path: Path):
            if file_path is None or not self._is_path_valid(file_path): self._ok_btn.deactivate()
            else: self._ok_btn.activate()
        self.event.listen(SELECT_FILE_EVENT, ok_btn_on_select_file_event)

        # Nav Bar
        def go_up(): self.event.emit(SET_DIR_EVENT, self._dir.parent)
        up_btn = Button(self.win, text="^", cmd=go_up, **self._nav_bar_btn_theme)

        self._path_input = TextInputLine(self.win, **self._nav_bar_path_theme)
        def path_input_on_set_dir_event(dir_path: Path):
            self._path_input.set_text(str(dir_path))
        def path_input_on_select_file_event(file_path: Path):
            self._path_input.set_text(str(file_path))

        def path_input_validate():
            if self._is_path_valid(Path(self._path_input.get_text())): self._ok_btn.activate()
            else: self._ok_btn.deactivate()
        def path_input_on_tab_key():
            self._path_input.deactivate()
        self.event.listen(SET_DIR_EVENT, path_input_on_set_dir_event)
        self.event.listen(SELECT_FILE_EVENT, path_input_on_select_file_event)
        self._path_input.on_key_enter = self._path_input_on_key_enter
        self._path_input.validate = path_input_validate
        self._path_input.on_tab_key = path_input_on_tab_key

        nav_bar = Frame(self.win, num_cols=2, **self._nav_bar_theme)
        nav_bar.add_grid_child(up_btn,           col=0)
        nav_bar.add_grid_child(self._path_input, col=1)

        # Explorer
        self.event.listen(SET_DIR_EVENT, self._explorer_on_set_dir_event)

        # Action Buttons
        action_bar = Frame(self.win, num_cols=2, **self._action_bar_theme)
        action_bar.add_grid_child(cancel_btn, col=0)
        action_bar.add_grid_child(self._ok_btn,     col=1)

        # add to Window
        self.win.add_grid_child(nav_bar,    row=0)
        # explorer frame will be added when the SET_DIR_EVENT fires
        self.win.add_grid_child(action_bar, row=2)

        # cancel when the window closes normally
        def win_on_close_request(): self._cancel()
        # some keyboard shortcuts
        def win_on_keyboard_press_key(sdl2_keycode: int):
            if sdl2_keycode == SDLK_h and self.tk.keyboard.get_mod("ctrl"):
                self._show_hidden = not self._show_hidden
                self.event.emit(SET_DIR_EVENT, self._dir)
            elif sdl2_keycode == SDLK_ESCAPE: self._cancel()

            if self._path_input.is_active(): return
            elif sdl2_keycode == SDLK_TAB: self._path_input.activate()
            elif sdl2_keycode == SDLK_BACKSPACE: go_up()

        self.win.close = self._win_close
        self.win.on_close_request = win_on_close_request
        self.win.event.listen(KEYBOARD_PRESS_KEY, win_on_keyboard_press_key)

        self.tk.add_child(self.win)

        self.event.emit(SET_DIR_EVENT, initial_dir)

    def _win_close(self, update:bool=True):
        Window.close(self.win, update=update)
        self.tk.event.emit(PATH_OPEN, self)

    def _get_dir_content(self, dir_path: Path) -> list:
        paths = set()
        for file_type in self._file_types:
            # add files
            paths.update({
                p for p in dir_path.glob(file_type) if not p.is_dir()
            })
        # Sort files by name. Ignore case
        sort_by_name = lambda path: path.name.lower()
        sorted_files = sorted(paths, key=sort_by_name)
        # add directories
        sorted_dirs = sorted([p for p in dir_path.iterdir() if p.is_dir()], key=sort_by_name)

        # directories before files
        sorted_paths = sorted_dirs + sorted_files

        # remove hidden files when needed
        if not self._show_hidden:
            sorted_paths = [p for p in sorted_paths if not p.name.startswith(".")]

        return sorted_paths

    def can_read(self, path: Path) -> bool:
        return os.access(path, os.R_OK)
