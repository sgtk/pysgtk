from sdl2 import SDLK_ESCAPE, SDLK_RETURN
from .. import SGTK, Window, Frame, Label, Button
from ..constants import USER_DECISION, KEYBOARD_PRESS_KEY

class AskYesOrCancel:
    def __init__(self, sgtk: SGTK,
        title:str="Are you sure?", text:str="Are you sure?",
        window_theme:dict=None, text_theme:dict=None,
        action_bar_theme:dict=None, action_bar_btn_theme:dict=None
    ):
        self.tk = sgtk
        self._window_theme         = window_theme or {}
        self._text_theme           = text_theme or {}
        self._action_bar_theme     = action_bar_theme or {}
        self._action_bar_btn_theme = action_bar_btn_theme or {}

        self._decision = False

        self._setup_win(title, text)

    def get_decision(self) -> bool:
        return self._decision

    def _setup_win(self, title: str, text: str):
        win = Window(self.tk, title=title, num_rows=2, **self._window_theme)

        def win_on_close_request():
            self.tk.event.emit(USER_DECISION, self)
            Window.on_close_request(win)
        win.on_close_request = win_on_close_request

        def cancel():
            self._decision = False
            win.on_close_request()
        def yes():
            self._decision = True
            win.on_close_request()

        text_label = Label(win, text=text, **self._text_theme)

        cancel_btn = Button(win, text="Cancel", cmd=cancel, **self._action_bar_btn_theme)
        yes_btn = Button(win, text="Yes", cmd=yes, **self._action_bar_btn_theme)

        action_frame = Frame(win, num_cols=2, **self._action_bar_theme)
        action_frame.add_grid_child(cancel_btn, col=0)
        action_frame.add_grid_child(yes_btn,    col=1)

        win.add_grid_child(text_label,   row=0)
        win.add_grid_child(action_frame, row=1)

        def win_on_keyboard_press_key(sdl2_keycode: int):
            if   sdl2_keycode == SDLK_ESCAPE: cancel()
            elif sdl2_keycode == SDLK_RETURN: yes()
        win.event.listen(KEYBOARD_PRESS_KEY, win_on_keyboard_press_key)

        self.tk.add_child(win)
