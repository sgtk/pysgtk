from pathlib import Path
from .BaseExplorer import BaseExplorer
from .. import SGTK

class OpenFile(BaseExplorer):
    def __init__(self, sgtk: SGTK, file_types: list, title:str="Open File",
        ok_btn_text:str="Open", start_dir:Path=None, show_hidden:bool=False,
        **theme
    ):
        super().__init__(sgtk, file_types, title, ok_btn_text=ok_btn_text,
            start_dir=start_dir, show_hidden=show_hidden, **theme
        )

    def _is_path_valid(self, path: Path) -> bool:
        if path is None: return None
        return path.exists() and path.is_file() and self.can_read(path)
