from .BaseWidget import BaseWidget, WIDGET_COLOR
from .Window import Window

class Widget(BaseWidget):
    def __init__(self, win: Window, color:tuple=WIDGET_COLOR,
        x:int=0, y:int=0, w:int=0, h:int=0, pad_x:int=0, pad_y:int=0,
        debug_name:str=""
    ):
        super().__init__(color=color, x=x, y=y, w=w, h=h,
            pad_x=pad_x, pad_y=pad_y, debug_name=debug_name
        )
        self.win = win
        self.setup_sgtk(win.tk, win.paint)
