from ctypes import POINTER, c_int, byref
from sdl2 import (
    SDL_Window, SDL_CreateWindow, SDL_DestroyWindow, SDL_WINDOWPOS_UNDEFINED,
    SDL_Renderer, SDL_CreateRenderer, SDL_DestroyRenderer, SDL_RENDERER_ACCELERATED,
    SDL_RENDERER_SOFTWARE, SDL_SetWindowSize, SDL_SetWindowPosition, SDL_WINDOWPOS_CENTERED,
    SDL_GetWindowID, SDL_RENDERER_TARGETTEXTURE, SDL_SetWindowTitle, SDL_WINDOW_HIDDEN,
    SDL_ShowWindow, SDL_RenderSetLogicalSize, SDL_SetWindowMinimumSize
)

from .SGTK import SGTK
from .Painter import Painter
from .Frame import Frame
from .EventManager import EventManager
from .constants import WINDOW_CLOSED

WINDOW_COLOR = (10, 10, 10)

class Window(Frame):
    def __init__(self, sgtk: SGTK, title: str, num_cols:int=1, num_rows:int=1,
        color:tuple=WINDOW_COLOR, debug_name:str=""
    ):
        self.win_id: int = -1
        self._title = title
        self._win: POINTER(SDL_Window)   = None
        self._ren: POINTER(SDL_Renderer) = None
        self._win_min_w = 100
        self._win_min_h = 50
        self._win_w = self._win_min_w
        self._win_h = self._win_min_h
        self._do_update_win_size = False
        self.event = EventManager()
        self._shown = False

        super().__init__(None, num_cols=num_cols, num_rows=num_rows, color=color, debug_name=debug_name)

        self._win = SDL_CreateWindow(self._title.encode(),
            SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
            round(self._win_w * sgtk.window_scale), round(self._win_h * sgtk.window_scale),
            SDL_WINDOW_HIDDEN
        )
        sgtk.check_sdl2_error("SDL_CreateWindow", do_exit=self._win is None)
        SDL_SetWindowMinimumSize(self._win, self._win_min_w, self._win_min_h)
        self.win_id = SDL_GetWindowID(self._win)

        renderer_args = SDL_RENDERER_TARGETTEXTURE
        renderer_args |= SDL_RENDERER_ACCELERATED if sgtk.accelerated else SDL_RENDERER_SOFTWARE
        self._ren = SDL_CreateRenderer(self._win, -1, renderer_args)
        sgtk.check_sdl2_error("SDL_CreateRenderer", do_exit=self._ren is None)
        SDL_RenderSetLogicalSize(self._ren, self._win_w, self._win_h)

        self.setup_sgtk(sgtk, Painter(self._ren, sgtk.font))
        self._render()
        self.paint.present()

    def center(self):
        SDL_SetWindowPosition(self._win,
            SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED
        )

    def get_title(self) -> str:
        return self._title
    def set_title(self, title: str):
        if title == self._title: return
        self._title = title
        SDL_SetWindowTitle(self._win, self._title.encode())

    def set_padding(self, pad_x: int, pad_y: int):
        super().set_padding(pad_x, pad_y)
        self._set_win_size(self._ow, self._oh)

    def set_inner_size(self, w: int, h: int):
        super().set_inner_size(w, h)
        self._set_win_size(self.get_recom_outer_w(), self.get_recom_outer_h())

    def set_outer_rect(self, ox: int, oy: int, ow: int, oh: int):
        super().set_outer_rect(ox, oy, ow, oh)
        self._set_win_size(self._ow, self._oh)

    def _set_win_size(self, w: int, h: int):
        if w < self._win_min_w: w = self._win_min_w
        if h < self._win_min_h: h = self._win_min_h
        if (w, h) == (self._win_w, self._win_h): return
        self._win_w = w
        self._win_h = h
        self._do_update_win_size = True

    def _check_update_win_size(self):
        if not self._do_update_win_size: return
        self._do_update_win_size = False
        SDL_RenderSetLogicalSize(self._ren, self._win_w, self._win_h)
        SDL_SetWindowSize(self._win, round(self._win_w * self.tk.window_scale), round(self._win_h * self.tk.window_scale))
        self.center()

    def _close(self, update:bool=True):
        self.tk.event.emit(WINDOW_CLOSED, self)
        super()._close(update=update)
        SDL_DestroyRenderer(self._ren)
        SDL_DestroyWindow(self._win)

    def _render(self):
        self.paint.clear(self._color)
    
    def render(self):
        self._check_update_win_size()

        super().render()
        if self.parent is not None and not self._shown:
            SDL_ShowWindow(self._win)
            self._shown = True

    def redraw(self):
        self._check_update_win_size()
        super().redraw()

    def on_close_request(self):
        self.close(update=False)
