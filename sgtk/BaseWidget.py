from .Painter import Painter

WIDGET_COLOR = (60, 60, 60)

class BaseWidget:
    id_count: int = 0
    def __init__(self, color:tuple=WIDGET_COLOR,
        x:int=0, y:int=0, w:int=0, h:int=0, pad_x:int=0, pad_y:int=0,
        debug_name:str=""
    ):
        self.tk: None # SGTK
        self.paint: Painter = None
        self.parent = None
        self.debug_name = debug_name

        self._color = color

        self._pad_x = 0 # padding between outer x and render x
        self._pad_y = 0 # padding between outer y and render y

        self._x = 0 # render x
        self._y = 0 # render y
        self._w = 0 # render w
        self._h = 0 # render h

        # outer rect in which the render rect must fit
        self._ox = 0
        self._oy = 0
        self._ow = 0
        self._oh = 0

        self.set_inner_size(w, h)
        self.set_padding(pad_x, pad_y)
        self.set_outer_rect(x, y, self.get_recom_outer_w(), self.get_recom_outer_h())

        BaseWidget.id_count += 1
        self._id = BaseWidget.id_count

    def get_id(self) -> int: return self._id

    def setup_sgtk(self, sgtk, painter: Painter):
        self.tk = sgtk
        self.paint = painter

    def set_parent(self, parent):
        """ called by parent """
        self.parent = parent

    def unset_parent(self):
        """ called by parent """
        self.parent = None

    def notify_parent_rect_changed(self):
        if self.parent is not None:
            self.parent.child_updated_rect(self)
        self.request_redraw()

    def request_redraw(self):
        if self.parent is not None:
            self.parent.child_requests_redraw(self)

    def get_recom_outer_w(self) -> int:
        return self._w + self._pad_x * 2

    def get_recom_outer_h(self) -> int:
        return self._h + self._pad_y * 2

    def set_padding(self, pad_x: int, pad_y: int):
        self._pad_x = pad_x
        self._pad_y = pad_y
        self._ow = self.get_recom_outer_w()
        self._oh = self.get_recom_outer_h()
        # update position
        self._calc_pos()
        self.notify_parent_rect_changed()

    def set_inner_size(self, w: int, h: int):
        self._w = w
        self._h = h
        self.notify_parent_rect_changed()

    def set_outer_rect(self, ox: int, oy: int, ow: int, oh: int):
        self._ox = ox
        self._oy = oy
        self._ow = ow
        self._oh = oh
        # update position
        self._calc_pos()
        self.notify_parent_rect_changed()

    def _calc_pos(self):
        # XXX set to top left
        self._x = self._ox + self._pad_x
        self._y = self._oy + self._pad_y

    def close(self, update:bool=True):
        if self.parent is not None: self.parent.remove_child(self, update=update)

    def redraw(self):
        self.render()

    def render(self):
        self.paint.rect(self._color, self._x, self._y, self._w, self._h)

    def get_widget_at(self, x: int, y: int):
        """ Returns: BaseWidget() or None """
        if (
            x >= self._x and x < self._x + self._w and
            y >= self._y and y < self._y + self._h
        ): return self
        else: return None

    def on_hover(self): pass
    def on_unhover(self): pass
    def on_mouse_down(self, button: int): pass

    def __repr__(self) -> str:
        name = type(self).__name__
        if self.debug_name: return f"{self.debug_name} <{name}>"
        return name
    __str__ = __repr__
