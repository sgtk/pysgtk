from sdl2 import (
    SDLK_LEFT, SDLK_RIGHT, SDLK_BACKSPACE,
    SDLK_DELETE, SDLK_HOME, SDLK_END,
    SDLK_RETURN, SDLK_TAB,
    SDLK_a, SDLK_c, SDLK_v, SDLK_x,
)
from .Label import (
    Label, LABEL_FONT_NAME, LABEL_FONT_SIZE, LABEL_COLOR,
    LABEL_BG_COLOR, LABEL_IPAD
)
from .Window import Window
from .constants import (
    MOUSE_LEFT, MOUSE_DOWN, MOUSE_MOTION,
    KEYBOARD_CHAR, KEYBOARD_PRESS_KEY, SGTK_UPDATE
)

TEXT_INPUT_FILTER_ALL = lambda _text: True
def TEXT_INPUT_FILTER_POSITIVE_INT(text: str):
    if text == "": return True
    try: v = int(text)
    except ValueError: return False
    return v >= 0

TEXT_INPUT_LINE_MIN_W = 20
TEXT_INPUT_LINE_FILTER = TEXT_INPUT_FILTER_ALL
TEXT_INPUT_LINE_ACTIVE_BG_COLOR = (100, 100, 100)
TEXT_INPUT_LINE_CURSOR_COLOR = (255, 255, 255)
TEXT_INPUT_LINE_HIGHLIGHT_COLOR = (200, 200, 255, 160)

class TextInputLine(Label):
    def __init__(self, win: Window, text:str="",
        min_w:int=TEXT_INPUT_LINE_MIN_W,
        font_name:str=LABEL_FONT_NAME, font_size:int=LABEL_FONT_SIZE,
        input_filter:callable=TEXT_INPUT_LINE_FILTER,
        color:tuple=LABEL_COLOR, bg_color:tuple=LABEL_BG_COLOR,
        active_bg_color:tuple=TEXT_INPUT_LINE_ACTIVE_BG_COLOR,
        highlight_color:tuple=TEXT_INPUT_LINE_HIGHLIGHT_COLOR,
        cursor_color:tuple=TEXT_INPUT_LINE_CURSOR_COLOR,
        x:int=0, y:int=0, pad_x:int=0, pad_y:int=0,
        ipad_x:int=LABEL_IPAD, ipad_y:int=LABEL_IPAD
    ):
        self._min_w = min_w
        self._filter = input_filter
        self._default_bg_color = bg_color
        self._active_bg_color = active_bg_color
        self._cursor_color = cursor_color

        self._cursor_shown = False
        self._cursor_i = 0
        self._cursor_blink_rate_secs = 0.5
        self._cursor_last_blink_time = 0.0

        self._select_i: int = None
        self._highlight_color = highlight_color

        self._active = False
        self._next_input = None # TextInputLine

        super().__init__(win, text=text, font_name=font_name,
            font_size=font_size, color=color, bg_color=bg_color,
            x=x, y=y, pad_x=pad_x, pad_y=pad_y, ipad_x=ipad_x, ipad_y=ipad_y
        )
        if self._text != "": self._cursor_i = len(self._text)

    def on_key_enter(self): pass
    def on_tab_key(self): self.activate_next()
    def validate(self): pass

    def set_next(self, text_input_line):
        self._next_input = text_input_line

    def activate_next(self):
        if self._next_input is None: return
        self.deactivate()
        self._next_input.activate()

    def set_inner_size(self, w: int, h: int):
        if w < self._min_w: w = self._min_w
        super().set_inner_size(w, h)

    def set_text(self, text: str) -> bool:
        """ Returns `True` on success or `False` otherwise """
        success = self._filter(text)
        if success: super().set_text(text)
        self.validate()
        return success

    def on_hover(self): self.tk.mouse.set_cursor("ibeam")
    def on_unhover(self): self.tk.mouse.set_cursor("arrow")

    def is_active(self) -> bool: return self._active

    def on_mouse_down(self, button: int):
        # mouse clicked this widget
        if button != MOUSE_LEFT: return
        self.activate()

        index = self._get_index_from_mouse_x()
        self._set_cursor(index, self.tk.keyboard.get_mod("shift"))

    def _on_mouse_down_event(self, _button: int):
        if self.tk.mouse.selected is not self:
            self.deactivate()

    def _on_mouse_move_event(self, _data):
        if self.tk.mouse.is_pressed(MOUSE_LEFT):
            self._set_cursor(self._get_index_from_mouse_x(), True)

    def _on_keyboard_char_event(self, char: str):
        self._write_string(char)

    def _on_keyboard_press_key_event(self, sdl2_keycode: int):
        shift_pressed = self.tk.keyboard.get_mod("shift")
        ctrl_pressed  = self.tk.keyboard.get_mod("ctrl")

        if sdl2_keycode == SDLK_RETURN: self.on_key_enter()
        elif sdl2_keycode == SDLK_TAB: self.on_tab_key()

        elif sdl2_keycode == SDLK_a and ctrl_pressed:
            self._set_cursor(0, False)
            self._set_cursor(len(self._text), True)

        elif sdl2_keycode == SDLK_c and ctrl_pressed:
            self._copy_selection()

        elif sdl2_keycode == SDLK_x and ctrl_pressed:
            if self._select_i is not None:
                self._copy_selection()
                self._delete_char()

        elif sdl2_keycode == SDLK_v and ctrl_pressed:
            clipboard_text = self.tk.get_sys_clipboard()
            if clipboard_text is not None:
                self._write_string(clipboard_text)

        elif sdl2_keycode == SDLK_BACKSPACE:
            self._delete_char()

        elif sdl2_keycode == SDLK_DELETE:
            if self._cursor_i < len(self._text):
                if self._select_i is not None: self._delete_char()
                else:
                    self._set_cursor(self._cursor_i + 1, False)
                    self._delete_char()

        elif sdl2_keycode == SDLK_LEFT:
            if self._select_i is not None and not shift_pressed:
                self._set_cursor(self._get_selection_indices()[0], False)
            else:
                self._set_cursor(self._cursor_i - 1, shift_pressed)
        elif sdl2_keycode == SDLK_RIGHT:
            if self._select_i is not None and not shift_pressed:
                self._set_cursor(self._get_selection_indices()[1], False)
            else:
                self._set_cursor(self._cursor_i + 1, shift_pressed)

        elif sdl2_keycode == SDLK_HOME:
            self._set_cursor(0, shift_pressed)
        elif sdl2_keycode == SDLK_END:
            self._set_cursor(len(self._text), shift_pressed)

    def _on_sgtk_update(self, runtime_secs: float):
        # animate the blinking cursor
        if runtime_secs < self._cursor_last_blink_time + self._cursor_blink_rate_secs:
            return
        self._cursor_last_blink_time = runtime_secs
        self._cursor_shown = not self._cursor_shown
        self.request_redraw()

    def _write_string(self, string: str):
        self._pause_cursor_blink()

        # remove selected characters
        text = self._get_text_without_selection()
        if text != self._text:
            self._cursor_i = self._get_selection_indices()[0]

        len_before = len(text)
        if self.set_text(text[:self._cursor_i] + string + text[self._cursor_i:]):
            len_difference = len(self._text) - len_before
            self._set_cursor(self._cursor_i + len_difference, False)
        else: self.request_redraw()

    def _delete_char(self):
        self._pause_cursor_blink()

        # remove selected characters
        if self._select_i is not None:
            if self.set_text(self._get_text_without_selection()):
                self._set_cursor(self._get_selection_indices()[0], False)
            return

        i = self._cursor_i
        if i == 0: return
        if self.set_text(self._text[:i-1] + self._text[i:]):
            self._set_cursor(i - 1, False)
        else: self.request_redraw()

    def _set_cursor(self, index: int, select: bool):
        if index < 0: index = 0
        elif index > len(self._text): index = len(self._text)
        
        if select and self._select_i is None:
            self._select_i = self._cursor_i
        elif not select: self._select_i = None

        self._cursor_i = index
        if self._select_i == self._cursor_i: self._select_i = None

        self._pause_cursor_blink()
        self.request_redraw()

    def _copy_selection(self):
        if self._select_i is None: return
        select_start_i, select_end_i = self._get_selection_indices()
        self.tk.set_sys_clipboard(self._text[select_start_i : select_end_i])

    def _pause_cursor_blink(self):
        self._cursor_shown = True
        self._cursor_last_blink_time = self.tk.runtime_secs

    def activate(self):
        if self._active: return
        self._active = True
        self.win.event.listen(MOUSE_DOWN, self._on_mouse_down_event)
        self.win.event.listen(MOUSE_MOTION, self._on_mouse_move_event)
        self.win.event.listen(KEYBOARD_CHAR, self._on_keyboard_char_event)
        self.win.event.listen(KEYBOARD_PRESS_KEY, self._on_keyboard_press_key_event)
        self.tk.event.listen(SGTK_UPDATE, self._on_sgtk_update)
        self._bg_color = self._active_bg_color
        self.request_redraw()

    def deactivate(self):
        if not self._active: return
        self._active = False
        self.win.event.unlisten(MOUSE_DOWN, self._on_mouse_down_event)
        self.win.event.unlisten(MOUSE_MOTION, self._on_mouse_move_event)
        self.win.event.unlisten(KEYBOARD_CHAR, self._on_keyboard_char_event)
        self.win.event.unlisten(KEYBOARD_PRESS_KEY, self._on_keyboard_press_key_event)
        self.tk.event.unlisten(SGTK_UPDATE, self._on_sgtk_update)
        self._cursor_shown = False
        self._bg_color = self._default_bg_color
        self.request_redraw()

    def _get_real_x_from_index(self, index: int) -> int:
        return self._x + self._ipad_x + self.tk.font.size(
            self._text[:index], self._font_name, self._font_size
        )[0]

    def _get_index_from_mouse_x(self) -> int:
        mouse_x = self.tk.mouse.x
        text_start_x = self._x + self._ipad_x
        text_width = 0
        for i in range(len(self._text) + 1):
            prev_width = text_width
            text_width = self.tk.font.size(
                self._text[:i], self._font_name, self._font_size
            )[0]
            char_width = text_width - prev_width
            text_x = text_start_x + text_width - char_width // 2
            if text_x >= mouse_x: return i - 1
        return len(self._text)

    def _get_selection_indices(self) -> tuple:
        if self._select_i is None:
            return (self._cursor_i, self._cursor_i)
        elif self._select_i < self._cursor_i:
            return (self._select_i, self._cursor_i)
        else:
            return (self._cursor_i, self._select_i)

    def _get_text_without_selection(self) -> str:
        if self._select_i is None: return self._text
        select_start_i, select_end_i = self._get_selection_indices()
        return self._text[:select_start_i] + self._text[select_end_i:]

    def render(self):
        super().render()
        self._render_highlight()
        self._render_cursor()

    def _render_cursor(self):
        if not self._cursor_shown: return
        x = self._get_real_x_from_index(self._cursor_i)
        y = self._y + self._ipad_y
        w = 2
        h = self._h - self._ipad_y * 2
        self.paint.rect(self._cursor_color, x, y, w, h)

    def _render_highlight(self):
        if self._select_i is None: return

        select_start_i, select_end_i = self._get_selection_indices()
        start_x = self._get_real_x_from_index(select_start_i)
        end_x   = self._get_real_x_from_index(select_end_i)

        self.paint.rect(self._highlight_color,
            start_x, self._y + self._ipad_y,
            end_x - start_x, self._h - self._ipad_y * 2
        )

    def close(self, update:bool=True):
        self.deactivate()
        super().close(update=update)
