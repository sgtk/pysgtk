from .BaseWidget import BaseWidget, WIDGET_COLOR

class ParentWidget(BaseWidget):
    def __init__(self, color:tuple=WIDGET_COLOR,
        x:int=0, y:int=0, w:int=0, h:int=0, pad_x:int=0, pad_y:int=0,
        debug_name:str=""
    ):
        self._children = []
        self._redraw_queue = []
        self._do_redraw_self = False

        super().__init__(color=color, x=x, y=y, w=w, h=h,
            pad_x=pad_x, pad_y=pad_y, debug_name=debug_name
        )

    def get_children(self) -> tuple:
        return tuple(self._children)

    def add_child(self, child: BaseWidget):
        self._children.append(child)
        child.set_parent(self)

    def remove_child(self, child: BaseWidget, update:bool=True):
        self._children.remove(child)
        child.unset_parent()

    def child_updated_rect(self, child: BaseWidget):
        pass

    def request_redraw(self):
        # redraw every child recursively
        self._do_redraw_self = True
        for child in self._children:
            child.request_redraw()

        if self.parent is not None:
            self.parent.child_requests_redraw(self)

    def _request_redraw(self):
        super().request_redraw()

    def child_requests_redraw(self, child: BaseWidget):
        if child in self._redraw_queue: return
        self._redraw_queue.append(child)
        self._request_redraw()

    def render(self):
        self._render()
        for child in tuple(self._children): child.render()

    def _render(self):
        pass

    def redraw(self):
        if self._do_redraw_self:
            self._render()
            self._do_redraw_self = False

        while self._redraw_queue:
            child = self._redraw_queue.pop()
            if child in self._children:
                child.redraw()

    def _close(self, update:bool=True):
        super().close(update=update)

    def close(self, update:bool=True):
        for child in tuple(self._children): child.close(update=False)
        self._close(update=update)

    def get_widget_at(self, x: int, y: int) -> BaseWidget:
        """ Returns: BaseWidget() or None """
        # if (x, y) is not in this boundary, return None
        if super().get_widget_at(x, y) is None: return None
        # check if any of the children or their children is at (x, y)
        for child in tuple(self._children):
            widget = child.get_widget_at(x, y)
            if widget is not None: return widget
        # no child was at (x, y), so this widget is selected
        return self
