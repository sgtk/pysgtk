from .Widget import Widget
from .Window import Window

LABEL_TEXT = "Label"
LABEL_FONT_NAME = "sans"
LABEL_FONT_SIZE = 14
LABEL_COLOR = (255, 255, 255)
LABEL_BG_COLOR = (70, 70, 70)
LABEL_IPAD = 4

class Label(Widget):
    def __init__(self, win: Window, text:str=LABEL_TEXT,
        font_name:str=LABEL_FONT_NAME, font_size:int=LABEL_FONT_SIZE,
        color:tuple=LABEL_COLOR, bg_color:tuple=LABEL_BG_COLOR,
        x:int=0, y:int=0, pad_x:int=0, pad_y:int=0,
        ipad_x:int=LABEL_IPAD, ipad_y:int=LABEL_IPAD
    ):
        super().__init__(win, color=color, x=x, y=y,
            w=0, h=0, pad_x=pad_x, pad_y=pad_y, debug_name=text
        )

        self._text = text
        self.debug_name = self._text
        self._font_name = font_name
        self._font_size = font_size
        self._bg_color = bg_color
        self._ipad_x = ipad_x
        self._ipad_y = ipad_y

        self.set_text(text)

    def set_text(self, text: str) -> bool:
        """ Returns `True` on success or `False` otherwise """
        self._text = text
        self.debug_name = self._text

        iw, ih = self.tk.font.size(self._text, self._font_name, self._font_size)
        self.set_inner_size(
            iw + self._ipad_x * 2,
            ih + self._ipad_y * 2
        )
        return True

    def set_bg_color(self, bg_color: tuple):
        self._bg_color = bg_color
        self.request_redraw()

    def get_text(self) -> str:
        return self._text

    def render(self):
        # draw background
        self.paint.rect(self._bg_color, self._x, self._y, self._w, self._h)
        # draw text
        self.paint.text(self._text, self._font_name, self._font_size, self._color,
            self._x + self._ipad_x, self._y + self._ipad_y
        )
