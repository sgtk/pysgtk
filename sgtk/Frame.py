from .ParentWidget import ParentWidget
from .BaseWidget import BaseWidget

FRAME_COLOR = (30, 30, 30)

class Frame(ParentWidget):
    def __init__(self, win, num_cols:int=1, num_rows:int=1, color:tuple=FRAME_COLOR,
        x:int=0, y:int=0, w:int=0, h:int=0, pad_x:int=0, pad_y:int=0,
        debug_name:str=""
    ):
        """ win: Window() - _can_ be None """

        self._num_cols = num_cols
        self._num_rows = num_rows

        self._grid = [[None for _ in range(self._num_cols)] for _ in range(self._num_rows)]

        self._is_updating_child_rects = False

        super().__init__(color=color, x=x, y=y, w=w, h=h, pad_x=pad_x, pad_y=pad_y, debug_name=debug_name)
        if win is not None: self.setup_sgtk(win.tk, win.paint)

    def get_num_rows(self) -> int: return self._num_rows
    def get_num_cols(self) -> int: return self._num_cols

    def child_updated_rect(self, child: BaseWidget):
        if self._is_updating_child_rects: return
        self._is_updating_child_rects = True
        self._update_children_outer_rect()

        self.set_inner_size(self._calc_inner_w(), self._calc_inner_h())
        self._is_updating_child_rects = False

    def _get_col_w(self, col_i: int) -> int:
        w = 0
        for row_i in range(self._num_rows):
            child = self._grid[row_i][col_i]
            if child is None: continue
            child_w = child.get_recom_outer_w()
            if child_w > w: w = child_w
        return w

    def _get_row_h(self, row_i: int) -> int:
        h = 0
        for col_i in range(self._num_cols):
            child = self._grid[row_i][col_i]
            if child is None: continue
            child_h = child.get_recom_outer_h()
            if child_h > h: h = child_h
        return h

    def _calc_inner_w(self) -> int:
        iw = 0
        for col_i in range(self._num_cols):
            iw += self._get_col_w(col_i)
        return iw

    def _calc_inner_h(self) -> int:
        ih = 0
        for row_i in range(self._num_rows):
            ih += self._get_row_h(row_i)
        return ih

    def _update_children_outer_rect(self):
        grid_y = self._y
        for row_i in range(self._num_rows):
            row = self._grid[row_i]
            row_h = self._get_row_h(row_i)
            grid_x = self._x
            for col_i in range(self._num_cols):
                child = row[col_i]
                col_w = self._get_col_w(col_i)
                if child is not None:
                    child.set_outer_rect(grid_x, grid_y, col_w, row_h)
                grid_x += col_w
            grid_y += row_h

    def add_grid_child(self, child: BaseWidget, col:int=0, row:int=0):
        self._grid[row][col] = child
        self.add_child(child)
        self.child_updated_rect(child)

    def get_grid_child(self, col:int=0, row:int=0) -> BaseWidget:
        """ Returns None if the field is empty """
        return self._grid[row][col]

    def remove_child(self, child: BaseWidget, update:bool=True):
        # remove child from grid
        for row in self._grid:
            if child in row:
                row[row.index(child)] = None
                break
        super().remove_child(child, update=False)
        if update: self.child_updated_rect(child)

    def _render(self):
        self.paint.rect(self._color, self._x, self._y, self._w, self._h)

    def set_outer_rect(self, ox: int, oy: int, ow: int, oh: int):
        super().set_outer_rect(ox, oy, ow, oh)
        # update children
        self._update_children_outer_rect()

    def set_padding(self, pad_x: int, pad_y: int):
        super().set_padding(pad_x, pad_y)
        self._update_children_outer_rect()
