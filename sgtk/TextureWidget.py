from sdl2 import SDL_DestroyTexture

from .Widget import Widget
from .Window import Window

class TextureWidget(Widget):
    def __init__(self, win: Window, w: int, h: int, x:int=0, y:int=0,
        pad_x:int=0, pad_y:int=0, debug_name:str=""
    ):
        super().__init__(win, x=x, y=y, w=w, h=h, pad_x=pad_x, pad_y=pad_y, debug_name=debug_name)

        self._tex = self.paint.create_target_texture(w, h)

    def close(self, update:bool=True):
        super().close(update=update)
        SDL_DestroyTexture(self._tex)

    def render(self):
        self.paint.texture(self._tex, self._x, self._y, self._w, self._h)
