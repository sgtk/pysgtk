from sgtk import *
from sgtk.constants import MOUSE_RIGHT, PATH_OPEN, KEYBOARD_PRESS_KEY
from sdl2 import SDLK_RETURN, SDLK_t

tk = SGTK()

win = Window(tk, "PySGTK Test", num_rows=2)

class TestWidget(TextureWidget):
    def __init__(self, win: Window, pad_x:int=0, pad_y:int=0):
        super().__init__(win, 600, 500, pad_x=pad_x, pad_y=pad_y)

    def update(self):
        if self.parent is None: return
        mouse_x = self.tk.mouse.x - self._x
        mouse_y = self.tk.mouse.y - self._y

        with self.paint.target_texture(self._tex):
            self.paint.clear((200, 200, 255))
            if self.tk.mouse.selected is self:
                color = (0, 255, 0, 120) if self.tk.keyboard.is_pressed(SDLK_RETURN) else (255, 0, 0, 120)
                self.paint.rect_outline(color, 5, mouse_x-20, mouse_y-20, 40, 40)
        self.request_redraw()

# MENU BAR
menu_bar = Frame(win, num_cols=3)
menu_bar_bg_color = (47, 52, 63)
file_menu_button = Button(win, text="File", bg_color=menu_bar_bg_color)
edit_menu_button = Button(win, text="Edit", bg_color=menu_bar_bg_color)
help_menu_button = Button(win, text="Help", bg_color=menu_bar_bg_color)
menu_bar.add_grid_child(file_menu_button, col=0)
menu_bar.add_grid_child(edit_menu_button, col=1)
menu_bar.add_grid_child(help_menu_button, col=2)
win.add_grid_child(menu_bar, row=0)
# / MENU BAR

win_frame = Frame(win, 2, 1)

# MAIN FRAME
main_frame = Frame(win, 1, 2)
test_widget = TestWidget(win, pad_x=4, pad_y=4)

## BOTTOM BAR
bottom_bar = Frame(win, num_cols=3)

bottom_text = Label(win, text="Bottom text")
bottom_btn  = Button(win, text="Open File...", pad_x=4)
bottom_save_btn = Button(win, text="Save File...", cmd=lambda: SaveFile(tk, "*.json", ensure_file_type=True), pad_x=4)
def do_open_python_file():
    OpenFile(tk, ["*.py"], title="Open Python file")
bottom_btn.set_cmd(do_open_python_file)

def on_path_opened(explorer_dialog):
    info_win = Window(tk, title="Info", num_rows=2)
    info_win.add_grid_child(Label(info_win,
        f'You opened the path: "{explorer_dialog.get_path()}"'
    ), row=0)
    info_win.add_grid_child(Button(info_win,
        text="OK", cmd=info_win.on_close_request
    ), row=1)
    tk.add_child(info_win)
tk.event.listen(PATH_OPEN, on_path_opened)

bottom_bar.add_grid_child(bottom_text,     col=0)
bottom_bar.add_grid_child(bottom_btn,      col=1)
bottom_bar.add_grid_child(bottom_save_btn, col=2)
## / BOTTOM BAR

main_frame.add_grid_child(test_widget, row=0)
main_frame.add_grid_child(bottom_bar, row=1)
# / MAIN FRAME

# SIDE BAR
side_bar = Frame(win, num_rows=3, pad_x=4)

side_text = Label(win, text="Side bar text!", font_size=24, pad_y=4)
side_widget = Widget(win, color=(255, 200, 200), w=160, h=160)
## close side_bar if the side_widget was right-clicked
side_widget.on_mouse_down = (
    lambda button: side_bar.close() if button == MOUSE_RIGHT else None
)

side_input = TextInputLine(win, pad_y=4, min_w=120)
side_bar.add_grid_child(side_text, row=0)
side_bar.add_grid_child(side_widget, row=1)
side_bar.add_grid_child(side_input, row=2)
# / SIDE BAR

win_frame.add_grid_child(main_frame, col=0)
win_frame.add_grid_child(side_bar, col=1)

win.add_grid_child(win_frame, row=1)

def win_on_keyboard_press_key(sdl2_keycode: int):
    if sdl2_keycode == SDLK_t: AskYesOrCancel(tk)
win.event.listen(KEYBOARD_PRESS_KEY, win_on_keyboard_press_key)

tk.add_child(win)

def tk_update():
    test_widget.update()
tk.update = tk_update

tk.create_graph()

tk.run()
